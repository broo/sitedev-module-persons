<?php

namespace Core\Sitedev\Persons\Model;

abstract class Person extends \App\ActiveRecord
{
    public function __construct()
    {
        parent::__construct('person');

        $this->addPrimaryKey('integer');
        $this->addAttr('object_type_id', 'integer');
        $this->addAttr('object_id', 'integer');
        $this->addAttr('status_id', 'integer');
        $this->addAttr('last_name', 'string');
        $this->addAttr('first_name', 'string');
        $this->addAttr('middle_name', 'string');
        $this->addAttr('nickname', 'string');
    }

    public function create()
    {
        if (!$this->_objectTypeId) {
            $this->_objectTypeId = static::OBJECT_TYPE_ID;
        }

        return parent::create();
    }

    /**
     * @param array $_where
     * @param array $_params
     * @return self[]
     */
    public static function getList($_where = null, $_params = null)
    {
        $where = empty($_where) ? array() : $_where;
        $where['object_type_id'] = static::OBJECT_TYPE_ID;

        $params = empty($_params) ? array() : $_params;
        if (!isset($params['order'])) {
            $params['order'] = 'CONCAT_WS("", last_name, first_name, middle_name, nickname)';
        }

        return parent::getList($where, $params);
    }

    public function getTitle()
    {
        $title = trim($this->lastName . ' ' . $this->firstName);
        if (!$title) $title = $this->nickname;

        return $title ?: parent::getTitle();
    }
}
