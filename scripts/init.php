<?php

use Ext\Db;

$wd = dirname(__FILE__) . '/';
require_once $wd . '../../../../core/src/libs.php';

initSettings();
$nl = PHP_EOL;

$sql = str_replace(
    '{$prfx}',
    Db::get()->getPrefix(),
    file_get_contents($wd . 'init.sql')
);

$queries = explode(';', $sql);

foreach ($queries as $query)
    if (trim($query))
        Db::get()->execute($query);

echo 'Готово';
echo $nl . $nl;
