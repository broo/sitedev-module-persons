SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


DROP TABLE IF EXISTS `{$prfx}person` ;

CREATE  TABLE IF NOT EXISTS `{$prfx}person` (
  `{$prfx}person_id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `object_type_id` TINYINT UNSIGNED NOT NULL ,
  `object_id` INT UNSIGNED NULL ,
  `status_id` TINYINT(3) UNSIGNED NULL ,
  `last_name` VARCHAR(255) NULL ,
  `first_name` VARCHAR(255) NULL ,
  `middle_name` VARCHAR(255) NULL ,
  `nickname` VARCHAR(255) NULL ,
  PRIMARY KEY (`{$prfx}person_id`) ,
  INDEX `fk_{$prfx}person_object_type_id_idx` (`object_type_id` ASC) ,
  INDEX `fk_{$prfx}person_object_id_idx` (`object_id` ASC)
--  CONSTRAINT `fk_{$prfx}person_object_type_id`
--    FOREIGN KEY (`object_type_id` )
--    REFERENCES `{$prfx}object_type` (`{$prfx}object_type_id` )
--    ON UPDATE CASCADE,
--  CONSTRAINT `fk_{$prfx}person_object_id`
--    FOREIGN KEY (`object_id` )
--    REFERENCES `{$prfx}object` (`{$prfx}object_id` )
--    ON DELETE CASCADE
--    ON UPDATE CASCADE
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
